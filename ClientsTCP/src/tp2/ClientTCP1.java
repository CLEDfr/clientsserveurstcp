package tp2;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

public class ClientTCP1 {

	private void client1() throws IOException {
		Socket  mySocket = new Socket("localhost", 2121); //1313
		InputStream in = mySocket.getInputStream();
		int c;
		while ((c = in.read() ) != -1) {
			System.out.print((char)c);
		}
		mySocket.close();
	}

	public static void main(String[] args) throws IOException {
		ClientTCP1 clt = new ClientTCP1();
		clt.client1();
	}
}
		
