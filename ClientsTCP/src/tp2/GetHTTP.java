package tp2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class GetHTTP {
	private void client1() throws IOException {
		Socket mySocket = new Socket("localhost", 80);
		BufferedReader in = new BufferedReader(new InputStreamReader(mySocket.getInputStream()));
		PrintWriter out = new PrintWriter(mySocket.getOutputStream(), true);
		String res = "GET / HTTP/1.1\r\nhost:localhost\r\n\r\n";
		String line;
		boolean vide = false;
		out.println(res);
		while ((line = in.readLine()) != null) {
			if (line.equals("")) {
				vide = true;
			}
			if (vide)
				System.out.println(line);

		}
		mySocket.close();
	}

	public static void main(String[] args) throws IOException {
		GetHTTP clt = new GetHTTP();
		clt.client1();

	}

}
