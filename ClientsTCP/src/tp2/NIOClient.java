package tp2;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class NIOClient {
	SocketChannel canal;  //SocketChannel.open(new InetSocketAddress(9876)); Bloquant
	int tailleEnvoye;
	ByteBuffer tampon = ByteBuffer.allocate(tailleEnvoye);
	// Connexion au serveur
	public void creation() throws IOException {
		canal = SocketChannel.open(); // creation du canal
		canal.configureBlocking(false); // non−blocking mode
		if (canal.connect(new InetSocketAddress(9876))) // connexion non bloquante
			System.out.println( "Connexion terminée " ) ;
		else
			System.out.println ( "Connexion non encore terminée " );
		while ( !canal.finishConnect() ) {
			System.out.println ( "Connexion toujours non terminée ") ;
		}
	}

	//Transmission
	public void transmission() throws IOException {
		int tailleRecu = canal.write(tampon); // Lecture data depuis un ByteBuffer
		if (tailleEnvoye == 0)
			System.out.println("Rien envoyé, est−ce normal ?");
		else
			System.out.println ("Octets envoyés : " + tailleEnvoye);
	}
	//reception
	public void reception() throws IOException {
		int tailleRecu =canal.read(tampon); // écriture data reçu dans un ByteBuffer
		switch ( tailleRecu ) {
		case -1:
			System.out.println("Fin du flot atteinte");
			canal.close();
			break ;
		case 0 :
			System.out.println("C’est vide");
			break ;
		default :
			System.out.println("Reçu : " + tampon.asCharBuffer()); // s i on a du t e x t e
		}

	}
}