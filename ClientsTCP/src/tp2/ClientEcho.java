package tp2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientEcho {
	private void client1() throws IOException {
		Socket mySocket = new Socket("localhost", 2121);
		BufferedReader in = new BufferedReader(new InputStreamReader(mySocket.getInputStream()));
		PrintWriter out = new PrintWriter(mySocket.getOutputStream(), true);
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
		String ligne;

		while (!(ligne = buf.readLine()).toUpperCase().equals("FIN") || !ligne.toUpperCase().equals(".")) {
			System.out.println(ligne);
			out.println(ligne);
		}
		out.close();
		mySocket.close();

	}

	public static void main(String[] args) throws IOException {
		ClientEcho clt = new ClientEcho();
		clt.client1();
	}
}