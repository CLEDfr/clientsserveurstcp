package tp1_2;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ClientUDP {
	private DatagramSocket dgSocket;

	ClientUDP() throws IOException {
		dgSocket = new DatagramSocket();

	}

	void go() throws IOException {
		DatagramPacket dgPacket = new DatagramPacket(new byte[0], 0, InetAddress.getLocalHost(), 9876);

		// System.out.println("client send from " + dgPacket.getSocketAddress());
		/*byte[] bufString = null;
		while (bufString == null) {
			System.in.read(bufString);
		}
		dgPacket.setData(bufString, 0, bufString.length);*/
		String str = "2testyryert";
		dgPacket.setData(str.getBytes());
		dgSocket.send(dgPacket);
		String s= new String(dgPacket.getData());
		System.out.println(s);
		DatagramPacket dgPacketR = new DatagramPacket(new byte[50], 50);
		dgSocket.receive(dgPacketR);
		System.out.println(new String(dgPacketR.getData(), dgPacketR.getOffset(), dgPacketR.getLength()));

	}

	public static void main(String[] args) throws IOException {
		new ClientUDP().go();

	}
}
