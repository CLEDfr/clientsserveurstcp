package tp1_2;

/**
 * @author <a href="mailto:jean.carle@univ-lille1.fr">Jean Carle</a>, IUT-A, Universite de Lille 1
 * 
 */

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class ServeurUDP {
	private DatagramSocket dgSocket;

	ServeurUDP(int pSrv) throws IOException {
		dgSocket = new DatagramSocket(pSrv);
	}

	void go() throws IOException {
		DatagramPacket dgPacket = new DatagramPacket(new byte[500], 500);
		String str;

		while (true) {
			dgSocket.receive(dgPacket);
			System.out.println("Datagram received from " + dgPacket.getSocketAddress());
			dgPacket.setSocketAddress(dgPacket.getSocketAddress());
			str = new String(dgPacket.getData(),dgPacket.getOffset(),dgPacket.getLength());;
			char c = str.charAt(0);
			StringBuilder res = new StringBuilder();
			if (!Character.isDigit(c)) {
				res.append("erreur sur le digit");
			} else if(c == '0') {
				res.append("0");
			} else {
				res.append(c);
				String [] tab = str.substring(1).split(" ");
				System.out.println(c);
				for (int i = 0; i < tab.length; i++) {
					for (int j = 0; j < Character.getNumericValue(c) ; j++) {
						res.append(tab[i] + " ");
					}
				}
			}
			dgPacket.setSocketAddress(dgPacket.getSocketAddress());;
			byte[ ]buffer = res.toString().getBytes( ) ;
			dgPacket.setData(buffer,  0 ,buffer.length) ;
			dgSocket.send(dgPacket) ;
		}
	}

	public static void main(String[] args) throws IOException {
		final int DEFAULT_PORT = 9876;
		new ServeurUDP(args.length == 0 ? DEFAULT_PORT : Integer.parseInt(args[0])).go();
	}
}