package tp1;

import java.net.*;
import java.util.*;

public class ex2 {

	public static void main(String[] args) throws SocketException {
		Enumeration<NetworkInterface> test = NetworkInterface.getNetworkInterfaces();
		while (test.hasMoreElements()) {
			go(test.nextElement());
		}

	}

	private static void go(NetworkInterface netInt) {
		List<InterfaceAddress> intAdd = netInt.getInterfaceAddresses();
		for (InterfaceAddress i : intAdd) {
			System.out.println(i.getAddress());
		}
	}
}
