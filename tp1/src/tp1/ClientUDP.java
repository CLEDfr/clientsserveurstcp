package tp1;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ClientUDP {
	private DatagramSocket dgSocket;

	ClientUDP() throws IOException {
		dgSocket = new DatagramSocket();

		
	}
	void go() throws IOException {
		DatagramPacket dgPacket = new DatagramPacket(new byte[0], 0,InetAddress.getLocalHost(),9876);
		String str;
			
			System.out.println("client send from " + dgPacket.getSocketAddress());
			str = "test" + "\n";
			byte[] bufDate = str.getBytes();
			dgPacket.setData(bufDate, 0, bufDate.length);
			dgSocket.send(dgPacket);

			DatagramPacket dgPacketR = new DatagramPacket(new byte[50], 50 );
			dgSocket.receive(dgPacketR);
			
			System.out.println(new String(dgPacketR.getData(),dgPacketR.getOffset(),dgPacketR.getLength()));
			
	}

	public static void main(String[] args) throws IOException {
		new ClientUDP().go();

	}
}

