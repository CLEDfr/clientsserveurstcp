package tp2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
//1. Daytime 13
//2. Echo 7
//3. Chargen 19
//4. Telnet 23
//5. SSH 22
//6. SMTP 25
//7. POP3 110
//8. FTP 21
//9. IMAP 2 et 3 143
public class SrvBeg {
	private ServerSocket serveurSocket = null;

	public SrvBeg(int port) {
		try {
			// Création de la Socket Serveur qui permettra d'attendre les connexions
			serveurSocket = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public void attente() {
		Socket unClient = null;

		// Boucle d'attente des clients
		while (true ) {
			try {
				// accept() est bloquant. Quand on en sort, on a un nouveau
				// client avec une nouvelle instance de socket
				unClient = serveurSocket.accept();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
			// quand on a un client, on peut instancier la
			// classe AccesService et lui demander de traiter
			// la requête.
			BufferedReader reception;
			try {
				reception = new BufferedReader(new InputStreamReader(unClient.getInputStream()));
				String envoi;
				PrintWriter envoyer = new PrintWriter(unClient.getOutputStream(), true);
				while((envoi = reception.readLine()) != null) {
					if(envoi.equals("FIN")) break;
					char c = envoi.charAt(0);
					StringBuilder answer = new StringBuilder();
					if (!Character.isDigit(c)) {
						answer.append("1Erreur : multiplicateur manquant.");
					} else if(c == '0') {
						answer.append("0");
					} else {
						answer.append("0");
						String [] tab = envoi.substring(1).split(" ");
						for (int i = 0; i < tab.length; i++) {
							for (int j = 0; j < Character.getNumericValue(c) ; j++) {
								answer.append(tab[i] + " ");
							}
						}
					}
					envoyer.println(answer);
				}
				
				reception.close();
				envoyer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		}

		// La classe doit être exécutée en passant le port serveur à utiliser en paramètre
		public static void main(String[] args) {
			SrvBeg serveur = new SrvBeg (9876);
			serveur.attente();
		}   
	}