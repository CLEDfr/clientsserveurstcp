package tp2;

import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Set;

public class NIOEcho {
	private static final int BUFFER_SIZE = 256;

	ServerSocketChannel serveur;
	InetSocketAddress resaAddr;
	Selector selector;
	ByteBuffer buffer;
	SocketChannel client;

	public NIOEcho() throws IOException {
		serveur = ServerSocketChannel.open();
		serveur.configureBlocking(false);
		resaAddr = new InetSocketAddress(9876);// ou 0 pour laisser le systeme trouver un port libre
		serveur.bind(resaAddr);
		selector = Selector.open();
		serveur.register(selector, SelectionKey.OP_ACCEPT);
		buffer = ByteBuffer.allocate(BUFFER_SIZE);

	}

	public void lecture() throws IOException {
		while (true) {
			selector.select();// Méthode bloquante
			Set<SelectionKey> listKeys = selector.selectedKeys();
			for (SelectionKey key : listKeys) {
				if (key.isAcceptable()) {
					client = serveur.accept(); // appel non bloquant:)
					client.configureBlocking(false); // canal non bloquant
					client.register(selector, SelectionKey.OP_READ); // enregistrement client
				} else if (key.isReadable()) {
					System.out.println("readfor: " + key.attachment());
					lire(key);
					
				} else if (key.isWritable())
					System.out.println("write: " + key.attachment());

				ecrire(key);
				listKeys.remove(key);
			}
		}
	}

	private void lire(SelectionKey key) throws IOException {
	}

	private void ecrire(SelectionKey key) throws IOException {

	}

	public static void main(String[] args) throws IOException {
		NIOEcho res = new NIOEcho();
		res.lecture();
	}
}