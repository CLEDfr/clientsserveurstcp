package tp2;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;

public class ClientTCP {
	SocketChannel canal;
	InetSocketAddress resaAddr;

	public void connexion() throws IOException {
		canal = SocketChannel.open();
		canal.configureBlocking(false); // non−blocking mode
		if (canal.connect(resaAddr)) // connexion non bloquante
			System.out.println("Connexion terminée");
		else
			System.out.println("Connexion non encore terminée ");
		while (!canal.finishConnect()) {
			System.out.println("Connexion toujours non terminée");
		}
	}
}
