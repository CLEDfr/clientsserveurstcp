package tp2;

import java.io.BufferedReader;
  


import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ServeurDayTime {
	ServerSocket mySocket;
	BufferedReader in;
	PrintWriter out;
	Socket client;

	ServeurDayTime(int port) throws IOException {
		mySocket = new ServerSocket(port);
		client = mySocket.accept();
		in = new BufferedReader(new InputStreamReader(client.getInputStream()));
		out = new PrintWriter(client.getOutputStream(), true);

	}

	void go() throws IOException {
		String str = new java.util.Date().toString();
		out.println(str);
		client.close();
	}

	public static void main(String[] args) throws IOException {
		ServeurDayTime serv = new ServeurDayTime(9876);
		serv.go();
	}
}