package tp2;
/**
 * M4102 : exemple de serveur TCP avec processus légers
 * @author <a href="mailto:jean.carle@univ-lille1.fr">Jean Carle</a>, IUT-A, Universite de Lille 1
 **/
import java.io.*;
import java.net.*;

public class Service {
	public static final int PORT_SERVICE = 9876;
	private ServerSocket s_Srv;

	Service() throws IOException {
		s_Srv = new ServerSocket(PORT_SERVICE);
	}

	// Réception clients et transfert vers un thread dédié
	private void attenteClient() throws IOException {
		Socket s_Clt;
		while(true) {
			s_Clt = s_Srv.accept();
			new ReponseTruc(s_Clt).start();
		}
	}

	// Test d'usage de la classe ... et rien d'autre
	public static void main(String [] args) throws IOException {
		Service srvTruc = new Service();
		srvTruc.attenteClient();
	}

	/**
	 * Gestion du protocole du service <<Truc>>
	 **/
	class ReponseTruc extends Thread {
		private Socket s_Client;

		ReponseTruc(Socket sClient) {
			this.s_Client = sClient;
			// Init des flots de données client ici
		}

		void dialogue () { /* le dialogue client/serveur ici */ 
			BufferedReader reception;
			try {
				reception = new BufferedReader(new InputStreamReader(s_Client.getInputStream()));
				String envoi;
				PrintWriter envoyer = new PrintWriter(s_Client.getOutputStream(), true);
				while((envoi = reception.readLine()) != null) {
					if(envoi.equals("FIN")) break;
					char c = envoi.charAt(0);
					StringBuilder answer = new StringBuilder();
					if (!Character.isDigit(c)) {
						answer.append("1Erreur : multiplicateur manquant.");
					} else if(c == '0') {
						answer.append("0");
					} else {
						answer.append("0");
						String [] tab = envoi.substring(1).split(" ");
						for (int i = 0; i < tab.length; i++) {
							for (int j = 0; j < Character.getNumericValue(c) ; j++) {
								answer.append(tab[i] + " ");
							}
						}
					}
					envoyer.println(answer);
				}

				reception.close();
				envoyer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		public void run() {
			dialogue();

			try {
				s_Client.close();
			} catch(IOException e) {}
		}
	}
}
